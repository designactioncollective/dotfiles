# What are these .dotfiles?

These are .dotfiles that Miguel uses in any project in order to keep the SCSS and Javascript consistently formatted. I've also included my package.json scripts because I use a few custom ones. I've also includes the aliases I use in my .bashrc file which helps me quickly type out some yarn and git commands.

---

## .prettierrc

| Location                | What it does                                                                                                                                                                                                                                                                                                                  |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Root of theme directory | Handles, whitespace, quotes, trailing commas and semicolons. We can extend it to auto-format a number of other things tho. This helps us pass the tests in `.eslintrc.json`. Probably will replace with [prettier on the npm library](https://www.npmjs.com/package/prettier) so that these .dotfiles can be editor-agnostic. |

## .eslintrc.js

| Location                | What it does                                                                                                                |
| ----------------------- | --------------------------------------------------------------------------------------------------------------------------- |
| Root of theme directory | This comes with Sage 9. When running `yarn build`, this file is used to make sure all our javascript is formatted correctly |

## .editorconfig

| Location                | What it does                                                                                                                                                                                                                                      |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Root of theme directory | I'm pretty sure this file also comes with Sage 9. It sets defaults for your coding editor, like whether to use tabs or spaces and what the indent size should be. In VS Code, I use it in conjunction with the "EditorConfig for VS Code" plugin. |

## .npmrc

| Location                | What it does                                                                                                                                                                          |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Root of theme directory | This isn't used for formatting but is important to know about cuz we often use Font Awesome. I'm not including the file here because it includes our security token for font awesome. |

## .stylintrc.js

| Location                | What it does                                                                                                                                                          |
| ----------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Root of theme directory | Pretty sure this one also comes with Sage 9. I changed some settings so that it matched the other lint/formatters here. Maybe changed double-quotes to single-quotes. |

## .csscomb.json

| Location                | What it does                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Root of theme directory | This one formats our (S)CSS, putting properties in a sensible order so that we can quickly scan a selector and understand what it's doing. I think it follows some of the rules of [idiomatic CSS](https://github.com/necolas/idiomatic-css) and this [CSS Rule Order](https://9elements.com/css-rule-order). Some day I might double-check that and re-arrange things to fit. We install this as a devDependency and run this command using `yarn csscomb`. I have an alias for this in my `.bashrc` file. |

## .package.json

| Location                | What it does                                                                                                                                                                                                                           |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Root of theme directory | I added a helper script and some devDependencies. Specifically I think I only added cssComb, but might need to double-check that. I include just the scripts and devDependencies that I use **_in addition_** to what Sage 9 includes. |

## .gitignore

| Location                 | What it does                                                    |
| ------------------------ | --------------------------------------------------------------- |
| Root of theme directory. | This one def comes with Sage 9. I include just a few additions. |

## .bashrc

| Location                   | What it does                                                                                                                                                                                                      |
| -------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ~/.bashrc on your computer | This file does all kinds of things to your terminal. If you use zsh or some other terminal, this might not mean anything to you. I'm not going to include my entire file, just the aliases. They really do help!! |

---

## Plugins that work with these .dotfiles.

As much as possible I chose to use toold that I could install using yarn, rather than tools that are installed using VS Code (which is the code editor I prefer). That said, there are a couple VS Code plugins I use. I'll list them here. The ones in bold are ones that work with a .dotfile.

- **Prettier Code Formatter** (Hopefully will replace with [the npm library](https://www.npmjs.com/package/prettier) soon)
- **Editor Config for VS Code** ([This is available for many code editors](https://editorconfig.org/))
- docs-markdown by Microsoft (Helps format README.md files)
- Dummy Text Generator
- Organize CSS
- SCSS Everywhere
- SCSS Refactoring
- Template Literal Editor

## The future

- [JSHint](https://jshint.com/docs/options/)?
- Replace CSSComb with one of the below?
  - [Stylefmt](https://github.com/morishitter/stylefmt): Specifically formats to Stylelint rules. CSSComb doesn't do this and it's sometimes an issue.
  - [Post](https://github.com/postcss/postcss): Like CSSComb but more popular?
  - Using the `-fix` flag? Would have fewer compatability issues.
  - But one of these options replace CSSComb's ability to sort rules.

## Gotchas

There are a few gotchas with this combination of tools. Below are ones I've come across.

- In general use `nth-of-type(1)` over `first-of-type()`, and `nth-last-of-type(1)` over `last-of-type()`.

### CSSComb will sometimes stall and complain of broken promises. I've seen this happen with these kinds of rules:

```scss
li {
  list-style: none;

  &:first-of-type() {
    @include media-breakpoint-down(sm) {
      border-left: 0;
    }
  }
}
```

```bash
yarn run v1.19.2
$ csscomb resources/assets/styles/
(node:10200) UnhandledPromiseRejectionWarning: Unhandled promise rejection (rejection id: 2): [object Object]
(node:10200) [DEP0018] DeprecationWarning: Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.
```

The line with `&:first-of-type()` causes this error. We can avoid this by removing the `&` and writing the ruleset like this:

```scss
li {
  list-style: none;
}

// Notice no `&` used
li:first-of-type() {
  @include media-breakpoint-down(sm) {
    border-left: 0;
  }
}
```

But sometimes this fails too. Alternatively we can also try to use a different pseudo-selector:

```scss
li {
  list-style: none;

  // No errors, does the same job
  &:nth-of-type(1) {
    @include media-breakpoint-down(sm) {
      border-left: 0;
    }
  }
}
```

### Stylelint complains that there isn't an empty line before an at-rule (like @include or @media)

```scss
#menu-footer {
  padding-inline-start: 11px;
  @include media-breakpoint-down(sm) {
    li:nth-of-type(1) {
      border-left: 0;
    }
  }
}
```

Stylelint: `42:3 ✖ Expected empty line before at-rule at-rule-empty-line-before`

We need to get a space between `padding-inline-start` and `@include...`. CSSComb keeps rewritting the stylesheet to remove the space. In this case I've found luck re-working the rule order, so that the breakpoint is _inside_ the selector, like this:

```scss
#menu-footer {
  padding-inline-start: 11px;

  // For whatever reason, CSSComb doesn't remove this empty line.
  li:nth-of-type(1) {
    @include media-breakpoint-down(sm) {
      border-left: 0;
    }
  }
}
```
