# This isn't my whole .bashrc file, just the aliases.
# You might need to edit your ~/.bash_profile, a
# profile for zsh, or another terminal.

alias gpp='git pull --rebase && git push'
alias gup='git pull --rebase'
alias gp='git push'
alias ga='git add'
alias gc='git commit'
alias gst='git status'
alias ycb='yarn csscomb && yarn build'
alias yc='yarn csscomb'
alias vstart='cd ~/vagrant-local && vagrant up'
